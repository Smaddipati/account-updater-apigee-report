class AccountUpdaterApi

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"
  require_relative '../../hmac-tools'


  def initialize()
    config = YAML.load_file('config.yaml')
    @base_url = config['url']
    @clientId =config['clientId']
    @apiSecret=config['apiSecret']
    @merchantId=config['merchantId']
    @merchantKey=config['merchantKey']

    puts "@base_url>>>>:#{@base_url}"
    set_url @base_url
  end

  def set_url(url)
    @url = url
  end


  def set_clientId()
    @clientId = @clientId
  end

  def set_clientKey()
    @apiSecret = @apiSecret
  end

  def set_merchantId()
    @merchantId = @merchantId
  end

  def set_merchantKey()
    @merchantKey= @merchantKey
  end

  #Perform Get operation
  def get(path)
    #Debug urls
    puts "path#{path}"
    full_url= @url+path
    puts "@full_url: #{full_url}"

    #calculate Hmac
    hmac = HmacTools.new
    authorization = hmac.hmac(@apiSecret, 'GET', full_url, '', @merchantId, nonce = DateTime.now.strftime('%Q'), timestamp = DateTime.now.strftime('%Q').to_i/1000)
    puts "HMAC : #{authorization}"

    #Do get action and verify the response
    begin
      @response =  RestClient.get full_url, :clientId => @clientId, :merchantId => @merchantId, :merchantKey => @merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue Exception => err
      @response = err.response
    end
    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
    puts "Response: #{@response}"
    puts "Response Code : #{@response.code}"

  end

  def verify_response_code(code)
    expect(@response.code.to_s).to eql(code)
  end

  def verify_responseheader_contenttype(contenttype)
    headers = @response.headers
    puts headers
    expect(headers[:content_type]).to include(contenttype)
    expect(@parsed_response['VaultResults'][0]['GUID'].strip.length).to eql 15
  end

  def verify_response_paramLength(key,value)
    expect(@parsed_response['VaultResults'][0][key].strip.length).to eql value.to_i

    #puts expect(@parsed_response).to be_an_instance_of(Array)
    #expect(@parsed_response['VaultResults'][0]['GUID'].strip).to eql("df45a22354b1472")
  end

  def verify_key_val(key, value)
    # puts "key:#{key} , value:#{value}"

    if (key == "responsecode")
      expect(@response.code.to_s).to eql(value)
    else
      expect(@parsed_response[key].strip).to eql(value)
    end
  end

end