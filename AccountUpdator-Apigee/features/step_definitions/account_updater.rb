Given(/^I set clientId header to `clientId`$/) do
  @accountUpdater_api = AccountUpdaterApi.new
  @accountUpdater_api.set_clientId
end

And(/^I set apiSecret to `apiSecret`$/) do
  @accountUpdater_api.set_clientKey
end


And(/^I set merchantId header to `merchantId`$/) do
  @accountUpdater_api.set_merchantId
end


And(/^I set merchantKey header to `merchantKey`$/) do
  @accountUpdater_api.set_merchantKey
end

When(/^I use HMAC and GET (.*)$/) do |path|
  @accountUpdater_api.get path
end


Then(/^response code should be (\d+)$/) do |code|
  @accountUpdater_api.verify_response_code code
end

Then(/^I will verify (.*) as (.*)$/) do |key, value|
  @accountUpdater_api.verify_key_val key, value
end

Then(/^response header Content-Type should be (.*)$/) do |contenttype|
  @accountUpdater_api.verify_responseheader_contenttype contenttype
end

And(/^response body should contain (.*) of (.*) digits$/) do |key,value|
  @accountUpdater_api.verify_response_paramLength key,value
end
