Feature: Account updater apigee report validation

  @get-account-updater
  Scenario: Verify the get account updater returning the credit card details
    Given I set clientId header to `clientId`
    And I set apiSecret to `apiSecret`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    When I use HMAC and GET /accountupdater/v1/accountupdater?startDate=2017-09-01&endDate=2017-09-01
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body should contain GUID of 15 digits
    And response body should contain Last4 of 4 digits
    And response body should contain ExpirationDate of 4 digits
        #And response body path $.VaultResults if of type Array

  @get-account-updater-withoutQueryParameter
  Scenario: Verify the get account updater returning the credit card details when query parameter not passed
    Given I set clientId header to `clientId`
    And I set apiSecret to `apiSecret`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    When I use HMAC and GET /accountupdater/v1/accountupdater
    Then response code should be 400
    And I will verify message as One or more required query parameters are missing
    And I will verify detail as Required query parameters: 'startDate' and 'endDate'


  @get-account-updater-InvalidStartDate
  Scenario: Verify the get account updater returning the credit card details when invalid StartDate is passed
    Given I set clientId header to `clientId`
    And I set apiSecret to `apiSecret`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    When I use HMAC and GET /accountupdater/v1/accountupdater?startDate=20qwe&endDate=2017-09-18
    Then response code should be 200

  @get-account-updater-InvalidEndDate
  Scenario: Verify the get account updater returning the credit card details when invalid end date is passed
    Given I set clientId header to `clientId`
    And I set apiSecret to `apiSecret`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    When I use HMAC and GET /accountupdater/v1/accountupdater?startDate=2017-08-01&endDate=20qw
    Then response code should be 200





