@intg
Feature: API proxy health
	As API administrator
    I want to monitor Apigee proxy and backend service health
    So I can alert when it is down
    
	@get-ping
    Scenario: Verify the API proxy is responding
        Given I set clientId header to `clientId`
        And I set apiSecret to `apiSecret`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /accountupdater/v1/ping
        Then I will verify responsecode as 200
        And I will verify apiproxy as account-updater-v1
        And I will verify message as PONG
        
